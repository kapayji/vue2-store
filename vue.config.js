module.exports = {
  publicPath:
    process.env.NODE_ENV === "production" ? "/examples/vue2-store/" : "/",
};
